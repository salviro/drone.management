﻿using Drone.Management.Domain.SeedWork;

namespace Drone.Management.Domain.AggregatesModel.DroneAggregate
{
    public class Drone : Entity, IAggregateRoot
    {
        private string _name;
        private string _description;
        private DateTime _warrantyEndDate;
        public Owner Owner { get; private set; }
        protected Drone() { }
        public Drone(string name, string description, DateTime warrantyExpirationDate, Owner owner)
        {
            _name = name;
            _description = description;
            _warrantyEndDate = warrantyExpirationDate;
            Owner = owner;
        }
    }
}
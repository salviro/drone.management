﻿using Drone.Management.Domain.SeedWork;

namespace Drone.Management.Domain.AggregatesModel.DroneAggregate
{
    public interface IDroneRepository : IRepository<Drone>
    {
        Drone Add(Drone drone);
        Task<bool> DeleteAsync(int droneId);
        Task<Drone> GetAsync(int droneId);
    }
}
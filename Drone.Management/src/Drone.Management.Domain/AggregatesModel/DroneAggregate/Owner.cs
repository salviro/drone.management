﻿using Drone.Management.Domain.SeedWork;

namespace Drone.Management.Domain.AggregatesModel.DroneAggregate
{
    public class Owner : Entity
    {
        private string _ownerName;
        private string _ownerAddress;
        public Owner() { }
        public Owner(string ownerName, string ownerAddress)
        {
            _ownerName = ownerName;
            _ownerAddress = ownerAddress;
        }
    }
}
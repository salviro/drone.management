﻿namespace Drone.Management.Domain.Exceptions
{
    public class DroneDomainException : Exception
    {
        public DroneDomainException() { }
        public DroneDomainException(string? message, Exception? innerException) : base(message, innerException) { }
    }
}
﻿using Drone.Management.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace Drone.Management.Infrastructure
{
    public class DroneContext : DbContext, IUnitOfWork
    {
        public DroneContext(DbContextOptions<DroneContext> options) : base(options) { }
        public DbSet<Domain.AggregatesModel.DroneAggregate.Drone> Drones { get; set; }
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            await base.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
﻿using Drone.Management.Domain.AggregatesModel.DroneAggregate;
using Drone.Management.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace Drone.Management.Infrastructure.Repositories
{
    public class DroneRepository : IDroneRepository
    {
        private readonly DroneContext _context;
        public IUnitOfWork UnitOfWork => _context;

        public DroneRepository(DroneContext context)
        {
            _context = context;
        }

        public Domain.AggregatesModel.DroneAggregate.Drone Add(Domain.AggregatesModel.DroneAggregate.Drone drone) => 
            _context.Drones.Add(drone).Entity;

        public async Task<Domain.AggregatesModel.DroneAggregate.Drone> GetAsync(int droneId) =>
            await _context.Drones
            .Include(x => x.Owner)
            .FirstOrDefaultAsync(o => o.Id == droneId);

        public async Task<bool> DeleteAsync(int droneId)
        {
            if (await GetAsync(droneId) is not Domain.AggregatesModel.DroneAggregate.Drone drone) return false;
            _context.Drones.Remove(drone);
            return true;
        }
    }
}
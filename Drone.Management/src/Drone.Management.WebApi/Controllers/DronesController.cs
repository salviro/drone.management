using Drone.Management.WebApi.Drones.Commands;
using Microsoft.AspNetCore.Mvc;

namespace Drone.Management.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DronesController : ApiControllerBase
    {
        public DronesController() { }
        [Route("add")]
        [HttpPost]
        public async Task<IActionResult> AddDrone([FromBody] AddDroneCommand addDroneCommand, CancellationToken cancellationToken) =>
            await Mediator.Send(addDroneCommand, cancellationToken) ? Ok() : BadRequest();

        [Route("delete")]
        [HttpDelete]
        public async Task<IActionResult> DeleteDrone([FromBody] DeleteDroneCommand deleteDroneCommand, CancellationToken cancellationToken) =>
            await Mediator.Send(deleteDroneCommand, cancellationToken) ? Ok() : BadRequest();

    }
}
﻿using System.Runtime.Serialization;

namespace Drone.Management.WebApi.Drones.Commands
{
    [DataContract]
    public class AddDroneCommand : ICommand
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string OwnerName { get; set; }
        [DataMember]
        public string OwnerAddress { get; set; }
        [DataMember]
        public DateTime WarrantyExpirationDate { get; set; }
    }
}
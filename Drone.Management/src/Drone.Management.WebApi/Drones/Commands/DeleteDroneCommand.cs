﻿using System.Runtime.Serialization;

namespace Drone.Management.WebApi.Drones.Commands
{   
    [DataContract]
    public class DeleteDroneCommand : ICommand
    {
        [DataMember]
        public int Id { get; set; }
    }
}
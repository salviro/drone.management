﻿using MediatR;

namespace Drone.Management.WebApi.Drones.Commands
{
    public interface ICommand : IRequest<bool>
    {
    }
}
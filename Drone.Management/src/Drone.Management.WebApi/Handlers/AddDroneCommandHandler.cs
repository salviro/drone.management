﻿using Drone.Management.Domain.AggregatesModel.DroneAggregate;
using Drone.Management.WebApi.Drones.Commands;
using MediatR;

namespace Drone.Management.WebApi.Handlers
{
    public class AddDroneCommandHandler
        : IRequestHandler<AddDroneCommand, bool>
    {
        private readonly IDroneRepository _droneRepository;
        private readonly ILogger<AddDroneCommandHandler> _logger;

        public AddDroneCommandHandler(IDroneRepository droneRepository,
            ILogger<AddDroneCommandHandler> logger)
        {
            _droneRepository = droneRepository;
            _logger = logger;
        }

        public async Task<bool> Handle(AddDroneCommand request, CancellationToken cancellationToken)
        {
            var owner = new Owner(request.OwnerName, request.OwnerAddress);
            var drone = new Domain.AggregatesModel.DroneAggregate.Drone(request.Name, request.Description, request.WarrantyExpirationDate, owner);
            _logger.LogInformation("Creating Drone {@Drone}", drone);
            _droneRepository.Add(drone);

            return await _droneRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }
}
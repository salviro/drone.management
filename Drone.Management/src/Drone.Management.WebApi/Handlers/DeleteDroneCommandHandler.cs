﻿using Drone.Management.Domain.AggregatesModel.DroneAggregate;
using Drone.Management.WebApi.Drones.Commands;
using MediatR;

namespace Drone.Management.WebApi.Handlers
{
    public class DeleteDroneCommandHandler
        : IRequestHandler<DeleteDroneCommand, bool>
    {
        private readonly IDroneRepository _droneRepository;
        private readonly IMediator _mediator;
        private readonly ILogger<DeleteDroneCommandHandler> _logger;

        public DeleteDroneCommandHandler(IMediator mediator,
            IDroneRepository droneRepository,
            ILogger<DeleteDroneCommandHandler> logger)
        {
            _droneRepository = droneRepository;
            _mediator = mediator;
            _logger = logger;
        }

        public async Task<bool> Handle(DeleteDroneCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Deleting DroneId {@Drone}", request.Id);
            await _droneRepository.DeleteAsync(request.Id);

            return await _droneRepository.UnitOfWork
                .SaveChangesAsync(cancellationToken) > 0;
        }
    }
}
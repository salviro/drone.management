using System.Reflection;
using Drone.Management.Domain.AggregatesModel.DroneAggregate;
using Drone.Management.Infrastructure;
using Drone.Management.Infrastructure.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddScoped<IDroneRepository, DroneRepository>();
builder.Services.AddDbContext<DroneContext>(x => x.UseInMemoryDatabase(databaseName: "drones_db"));

builder.Services.AddControllers();
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();